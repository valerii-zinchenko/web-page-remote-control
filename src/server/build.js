const fs = require('fs');

const {argv: [, , inputFile]} = process;

const file = fs.readFileSync(inputFile);

(async () => {
  const { default: processComponent } = await import('./processWebComponent.mjs');

  const content = processComponent(file, inputFile);

  console.log(content);
})();
