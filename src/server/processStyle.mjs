import sass from 'sass';


export function scss(content) {
    const result = sass.compileString(content, {
        loadPaths: [
            './src/controller'
        ],
        //sourceMap: true,
    });

    return result.css;
};
