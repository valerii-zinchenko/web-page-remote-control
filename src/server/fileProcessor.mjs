import processComponent from './processWebComponent.mjs';
import { scss } from './processStyle.mjs';


export default {
	'.scss': scss,
	'.html': processComponent,
};
