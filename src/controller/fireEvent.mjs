export default function fireEvent(eventName, detail) {
	document.body.dispatchEvent(new CustomEvent(eventName, {detail}));
}
