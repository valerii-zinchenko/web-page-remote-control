import fireEvent from './fireEvent.mjs';
import {encode, decode} from '../shared/wsData.mjs';
import wsMessageProcessor from '../shared/wsMessageProcessor.mjs';


const typeActionProcessorMap = {
	request: {},
	response: {},
	sync(obj) {
		Object.keys(obj).forEach(key => {
			fireEvent(`sync-${key}`, obj[key]);
		});
	},
	error(msg) {
		console.error(msg);
	}
};

const q = [];
let ws;
let initTimeout;

function initWebSocket() {
	if (ws) {
		ws.removeEventListener('message', onWSMessage);
		ws.removeEventListener('close', onWSClose);
		ws.removeEventListener('error', onWSError);
		ws.close();
	}
	if (initTimeout) {
		clearTimeout(initTimeout);
		initTimeout = undefined;
	}

	ws = new WebSocket(`ws://${location.host}`);

	ws.addEventListener('open', () => {
		for (; q.length > 0;) {
			ws.send(q.pop());
		}

		fireEvent('ws-reset');
	});

	ws.addEventListener('message', onWSMessage);
	ws.addEventListener('close', onWSClose);
	ws.addEventListener('error', onWSError);
}

function onWSMessage(event) {
	wsMessageProcessor(ws, typeActionProcessorMap, event.data);
}
function onWSClose(event) {
	initTimeout = setTimeout(initWebSocket, 5000);
	console.log(event);
}
function onWSError(event) {
	console.error(event);
}

initWebSocket();

window.addEventListener('visibilitychange', event => {
	if (ws.readyState === WebSocket.OPEN || document.hidden)
		return;

	clearTimeout(initTimeout);
	initWebSocket();
});

document.body.addEventListener('ws-request', ({detail: {action, args, toQueue = true}}) => {
	const data = encode({
		type: 'request',
		action,
		args
	});

	if (!ws && toQueue) {
		q.unshift(data);
		return;
	}

	switch (ws.readyState) {
		case WebSocket.CONNECTING:
			if (toQueue) {
				q.unshift(data);
			}
			break;
		case WebSocket.OPEN:
			ws.send(data);
			break;
	}
});

if (location.hash === '') {
    location.hash = '#touchpad';
}
