import {getItem, setItem} from './storage.mjs';


async function waitForSelector(selector, retry = 0) {
  const element = document.querySelector(selector);
  if (element) {
    return element;
  }

  if (retry > 5) {
    return null;
  }

  return new Promise(resolve => setTimeout(async () => {
    resolve(await waitForSelector(selector, retry+1));
  }, 200));
}

const sensitivityKey = 'settings-sensitivity';
const useNaturalScrollingKey = 'settings-useNaturalScrolling';
const useDoubleTapAndHoldForScrollingKey = 'settings-useDoubleTapAndHoldForScrolling';

(async () => {
  const [
    appSettings,
    touchpad,
  ] = await Promise.all([
    waitForSelector('app-controller-settings'),
    waitForSelector('app-controller-touchpad'),
  ]);

  const values = await Promise.all(
    [
      ['sensitivity', sensitivityKey, 1],
      ['use-natural-scrolling', useNaturalScrollingKey, false],
      ['use-double-tap-and-hold-for-scrolling', useDoubleTapAndHoldForScrollingKey, true],
    ].map(async ([attr, key, globalDefaultValue]) => {
      let value = await getItem(key);
      if (value === undefined) {
        value = appSettings.getAttribute(attr);
      }
      return [attr, value === null ? globalDefaultValue : value, key];
    })
  );

  values.forEach(([attr, value, key]) => {
    appSettings.addEventListener(`${attr}-change`, ({detail: { newValue }}) => {
      updateAttribute(touchpad, attr, newValue);
    });

    updateAttribute(appSettings, attr, value);

    appSettings.addEventListener(`${attr}-change`, ({detail: { newValue }}) => {
      newValue === undefined ? removeItem(key) : setItem(key, newValue);
    });
  });
})();

function updateAttribute(element, name, value) {
  if (value === false || value === undefined || value === null)
    element.removeAttribute(name);
  else
    element.setAttribute(name, value || '');
}
