export async function getItem(key) {
	const value = localStorage.getItem(key);
	return typeof value === 'string' ? JSON.parse(value) : undefined;
}

export async function setItem(key, value) {
	return localStorage.setItem(key, JSON.stringify(value));
}

export async function removeItem(key) {
	localStorage.removeItem(key);
}
