#!/usr/bin/env bash

# cd to the directory of this script
cd "${0%/*}"

# load ENV variables
set -o allexport
source ./.env
set +o allexport

set -euo pipefail

# npm is not found without this
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

case $XDG_SESSION_TYPE in
    wayland | x11)
        trap "echo -e '\nClosing...' && kill -- -$$" EXIT

        PATH=".venv/usr/bin:$PATH"
        YDOTOOL_SOCKET="${TMPDIR:-${TEMP:-${TMP:-/tmp}}}"/.pc-rc-ydotool-socket
        export PATH YDOTOOL_SOCKET

        # TODO: Find a clean way to kill ydotoold by exiting. Currently the background process will continue to work till the pc is restarted
        # If this script was exited and another one started - a new instance of ydotoold will not be created, the previous one will continue
        # to process the requests.
        sudo -b PATH=$PATH SOCKET_PATH=$YDOTOOL_SOCKET OWN="$(id -u):$(id -g)" bash -c 'ydotoold --socket-path=$SOCKET_PATH --socket-own=$OWN'
        npm start &
        npx pc-rc-widget
        ;;

    *)
        # do nothing
        # start is happening outside of the GUI, a user might be logged in from a terminal
esac
