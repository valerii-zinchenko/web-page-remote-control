#!/usr/bin/env bash

set -xe

./scripts/installs/ydotool.sh

# install JS packages
npm install

# set default settings
if [ ! -f .env ]; then
   cp .env{.template,}
fi

# copy runner script into the root
cp ./scripts/pc-rc.sh ./
