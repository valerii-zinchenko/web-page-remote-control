# PC remote controller

The main idea of this application is to use a smartphone as a remote controller for a PC.

This app does not communicates with external servers, only a connected network is used. Both, a PC and a controller (a smartphone or other device with a web-browser), must be in the same network.


## Requirements

- nodejs v16+

### Wayland and X11 desktops

Under the hood it is using [ydotool](https://github.com/ReimuNotMoe/ydotool/).

## Install, configure and run

1. `install.sh`

After installation a main runner file will be available in the root of the application `pc-rc.sh`.

App configurations could be found in `.env` file.


## Run

1. `./pc-rc.sh`
1. An address of web will be printed to console, like this
```sh
Client URLs:
	http://192.168.1.218:8080
```
1. Make sure your PC and controller (smartphone) are in same network.
1. Open one of the printed "Client URLs" on your controller
1. A web page should be open and actions should be visible on your PC.

